import os
import sys
import pickle
import sklearn
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow.compat.v1.keras.backend as K
from sklearn.model_selection import train_test_split
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import AbstractPipelineComponent
from tensorflow.keras.layers import Input, Embedding, Concatenate, Dense, Flatten, Reshape, BatchNormalization, Dropout

try:
    global_variable_1 = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/projectb/pickles/global_variable_1.pkl", "rb" ))
    global_variable_2 = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/projectb/pickles/global_variable_2.pkl", "rb" ))
except Exception as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xprparam_projectname = projectb
## $xprparam_componentname = component_one
## $xprparam_componenttype = pipeline_job
## $xprparam_global_variables = ["global_variable_1", "global_variable_2"]
## $xprjobparam_learningrate = 0.4

"""
This is the implementation of the first component
"""

    
class FirstComponent(AbstractPipelineComponent):

    def __init__(self):
      pass

if __name__ == "__main__":
    print('main section')

try:
    pickle.dump(global_variable_1, open("/mnt/nfs/data/jupyter_experiments/projects/projectb/pickles/global_variable_1.pkl", "wb"))
    pickle.dump(global_variable_2, open("/mnt/nfs/data/jupyter_experiments/projects/projectb/pickles/global_variable_2.pkl", "wb"))
except Exception as e:
    print(str(e))

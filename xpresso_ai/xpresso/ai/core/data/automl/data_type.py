from enum import Enum


class DataType(Enum):
    """
    Enum class to standardize all the datatype
    """
    ORDINAL = "ordinal"
    NOMINAL = "nominal"
    NUMERIC = "numeric"
    STRING = "string"
    TEXT = "text"
    DATE = "date"

    FLOAT = "float64"
    INT = "int"
    OBJECT = "object"
    BOOL = "bool"
    PD_DATETIME = "datetime"

    def __str__(self):
        return self.value
